@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href="#">click this link</a>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad atque autem ducimus eos id,
                            inventore ipsum molestiae nesciunt quas repellendus saepe, sed vitae voluptas. Eaque magni
                            ut vero. Itaque, officiis.</p>
                        <button class="btn btn-primary bg-primary-grad">Testing</button>
                        <button class="btn">Testing</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
